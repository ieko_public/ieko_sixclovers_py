import os
from dotenv import load_dotenv
load_dotenv(override=True)

# data for make requests to SixClovers
BASE_URL = os.getenv("BASE_URL")
PUBLIC_API_KEY = os.getenv("PUBLIC_API_KEY")
PRIVATE_API_KEY = os.getenv("PRIVATE_API_KEY")

# public addresses that make up the multi-sig wallet
# the first one is the one created by SixClovers once the multi-sig is setted up
SIGNER_PUBLIC_ADDRESS_1 = os.getenv("SIGNER_PUBLIC_ADDRESS_1")
SIGNER_PUBLIC_ADDRESS_2 = os.getenv("SIGNER_PUBLIC_ADDRESS_2")
SIGNER_PUBLIC_ADDRESS_3 = os.getenv("SIGNER_PUBLIC_ADDRESS_3")

# private key of one wallet that belongs to multi-sig
SIGNER_PRIVATE_KEY = os.getenv("SIGNER_PRIVATE_KEY")

# data for new currency script
NEW_CURRENCY = os.getenv("NEW_CURRENCY")
WALLET_ID = os.getenv("WALLET_ID")

# data for withdrawal script
WITHDRAWAL_AMOUNT = os.getenv("WITHDRAWAL_AMOUNT")
WITHDRAWAL_CURRENCY = os.getenv("WITHDRAWAL_CURRENCY")
WALLET_RECEIVER = os.getenv("WALLET_RECEIVER")
BC_INTEGRATION_ID = os.getenv("BC_INTEGRATION_ID")

# data for transfer script
TRANSFER_AMOUNT = os.getenv("TRANSFER_AMOUNT")
TRANSFER_CURRENCY = os.getenv("TRANSFER_CURRENCY")
EMAIL_IDENTITY_RECEIVER = os.getenv("EMAIL_IDENTITY_RECEIVER")
ORGANIZATION_ID_RECEIVER = os.getenv("ORGANIZATION_ID_RECEIVER")
ORIGINATOR_ACCOUNT_ID = os.getenv("ORIGINATOR_ACCOUNT_ID")
PUBLIC_API_KEY_RECEIVER = os.getenv("PUBLIC_API_KEY_RECEIVER")
PRIVATE_API_KEY_RECEIVER = os.getenv("PRIVATE_API_KEY_RECEIVER")