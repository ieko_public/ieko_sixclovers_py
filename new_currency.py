import requests
from algorand_sdk import AlgorandSDK
from utilities import exec_and_catch, check_needed_data
from settings import (
    NEW_CURRENCY, WALLET_ID, PUBLIC_API_KEY, PRIVATE_API_KEY, BASE_URL
)

needed_data = [NEW_CURRENCY, WALLET_ID, PUBLIC_API_KEY, PRIVATE_API_KEY, BASE_URL]
# Check if all required data is filled
check_needed_data(needed_data)

# Authentication data for SixClovers
authentication = (PUBLIC_API_KEY, PRIVATE_API_KEY)
add_currency_endpoint = f"wallets/{WALLET_ID}/currency"
payload = {
    "currency": NEW_CURRENCY
}

# Post new currency to SixClovers
new_currency_response = requests.post(
    BASE_URL + add_currency_endpoint,
    auth=authentication,
    json=payload
)
if new_currency_response.status_code not in [200, 202]:
    raise Exception(
        f"Error {new_currency_response.status_code} - {new_currency_response.json()}"
    )

transaction = new_currency_response.json()
# Check if the currency was already added to wallet
if "signaturePayload" not in transaction:
    raise Exception(f"Currency {NEW_CURRENCY} already added to that wallet")

# Get the signature payload to commit the transaction
signature_payload = [transaction["signaturePayload"]]

# Decoding, signing and encoding payload with Algorand SDK
decoded_transactions = exec_and_catch(signature_payload, AlgorandSDK.decodeSignaturePayloads)
signed_transactions = exec_and_catch(decoded_transactions, AlgorandSDK.signAndEncodeTransactions)

# Some error handling
if len(signed_transactions) != 1 or not signed_transactions[0]:
    raise Exception("Error signing transactions")

# Commit new currency SixClovers endpoint
commit_currency_endpoint = f"wallets/{WALLET_ID}/currency/commit"
commit_payload = {
    "referenceToken": transaction["referenceToken"],
    "signaturePayload": signed_transactions[0]
}
commit_response = requests.post(
    BASE_URL + commit_currency_endpoint,
    auth=authentication,
    json=commit_payload
)
if commit_response.status_code not in [200, 202]:
    raise Exception(f"Error {commit_response.status_code} - {commit_response.json()}")

print(f"Sent commit successfully - status {commit_response.status_code}")
