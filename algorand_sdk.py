from algosdk import encoding
from algosdk.future import transaction
from settings import (
    SIGNER_PUBLIC_ADDRESS_1, SIGNER_PUBLIC_ADDRESS_2,
    SIGNER_PUBLIC_ADDRESS_3, SIGNER_PRIVATE_KEY
)
from utilities import check_needed_data

needed_data = [SIGNER_PUBLIC_ADDRESS_1, SIGNER_PUBLIC_ADDRESS_2, SIGNER_PUBLIC_ADDRESS_3, SIGNER_PRIVATE_KEY]
check_needed_data(needed_data)

# create the multi-sig account object
version = 1 # multi-sig version
threshold = 2 # how many signatures are necessary
msig = transaction.Multisig(
    version,
    threshold,
    [SIGNER_PUBLIC_ADDRESS_1, SIGNER_PUBLIC_ADDRESS_2, SIGNER_PUBLIC_ADDRESS_3]
)

class AlgorandSDK:
    def decodeSignaturePayloads(listPayloads):
        # return decoded transaction objects from payloads given by SixClovers
        try:
            return {
                "error": False,
                "data": [
                    encoding.msgpack_decode(payload)
                    for payload in listPayloads
                ]
            }
        except:
            return {
                "error": True,
                "data": "AlgorandSDK - Error decoding payloads"
            }

    def signAndEncodeTransactions(listDecodedTxs):
        # for each transaction, it creates a MultisigTransaction object and signs it. Then, encodes and returns the list
        signedTxs = []
        try:
            for tx in listDecodedTxs:
                if tx.sender == msig.address():
                    # convert tx to multisig transaction
                    multisigTx = transaction.MultisigTransaction(tx, msig)
                    # sign transaction
                    multisigTx.sign(SIGNER_PRIVATE_KEY)
                    # encode and add to list of signed transactions
                    signedTxs.append(
                        encoding.msgpack_encode(multisigTx)
                    )

            return {
                "error": False,
                "data": signedTxs
            }

        except:
            return {
                "error": True,
                "data": "AlgorandSDK - Error signing transactions"
            }
