## IEKO Python scripts using Six Clovers' decentralized payments network

**Hi! Welcome to IEKO**

We are software developers specialized in the financial market.
We propose a new paradigm of **Sustainable Software.**

**HERE we are going to share with you the experience obtained by integrating one of our clients with SixClovers Technology**

Through a scheme of GIT repositories, we constantly **release knowledge** and subproducts
to be shared, reused and improved by the participants of the ecosystem.
The contributions received as feedback are evaluated and, if they are considered valuable,
incorporated.

The IEKO project is under development. **We invite you to participate**, there is a lot to
discover together. 

Version 1.0 IEKO 2022

## REPO OBJECTIVE

The objective of this repository is to guide any developer in the integration of Six Clovers to their financial solutions from the use of Python as a programming language. To do this, we will rely on three fundamental operations: adding a new currency to our wallet, making withdrawals and transferring funds between Six-Clovers organizations.

**You can find the full POST explaining this example development [HERE](COMPLETAR LINK TO POST)**

## INTRO

To understand the architecture of the solution proposed in this project, we will take the withdrawal of funds as an example operation. When we send a request to withdraw funds to the Six Clovers API, it will return a response and the "signature payloads", that is, the body of the response contains the information of the set of transactions that represents the withdrawal of funds. The body of the response is made up of a string or a set of simple strings which represent encrypted transactions and the information associated with them. Taking these definitions into account, our architecture should respond to the following flow:
* Decode the response body and get the transactions.
* Create multi-signature transactions.
* Sign multi-signature transactions.
* Encrypt the transactions again.
* Send the result to Six Clovers.

The first step is to copy the **‘.env_example’** file and rename it as **‘.env’** to work as an environment file. The goal is to fill it with the needed data properly described there. Then, ensure you have installed [Python](https://www.python.org/downloads/release/python-390/) (version 3.9) and [pipenv](https://pypi.org/project/pipenv/). After that, run the following command:

```pipenv install```

This will create a new environment to use the script with the needed libraries. To run a file, you have to use:

```pipenv run python <script_name>```\
Where <script_name> could be new_currency, transfer or withdrawal.

Things to consider:
In the case of adding a new currency (for example adding USDC) you have to get some funds in your wallet. In testnet, we used this Algorand faucet. To use it, simply paste there your multi signature address created in Six Clovers and once you click “Dispense”, you will obtain 10 ALGOs.
In the case of transfer, this means internal, so you will need to have another organization in Six Clovers.
In the case of withdrawals, you may have to add the address you want to withdraw to “Withdrawal Addresses” (in Six Clovers, inside your organization configuration).

After this, we must install the package by console by executing:

```pip3 install py-algorand-sdk```

In our repository you will find three complete examples of how to add a new currency to a wallet, withdraw funds and transfer between two Six Clovers organizations. In the “algorand_sdk.py” file, you will find some functions that use the py-algorand-sdk library.


## We are [IEKO](http://ieko.io/)

We provide software solutions for *Fintech business* such as: Consulting, Custom\
Developments, Algorithmic Trading, Digital Onboarding, APIfication and integration\
of data sources.

Our goal is to build software being responsible with the technological ecosystem in\
which we exist, contributing to financial inclusion, making a concrete contribution to\
economic and social development.

#### Get in touch with us and know our projects.

* [ieko.io](http://ieko.io/)
* [LinkedIn](https://www.linkedin.com/company/ieko-io)
* [Instagram](https://www.instagram.com/ieko.io/)
* [Twitter](https://twitter.com/ieko_io)
* [Facebook](https://www.facebook.com/ieko.io)
* contacto@ieko.io 

From Rosario, Argentina, to the world\
**#WeLoveToBuildSoftware**\
**#WeBuildSustainableSoftware**


_This project is licensed under the **GNU Lesser General Public License v3.0.**_ [Learn more](https://choosealicense.com/licenses/lgpl-3.0/)
_This project is shared under_ [***CopyLeft LGPL** license*](https://gitlab.com/ieko_public/ieko_proceso_publico/-/blob/master/LICENSE). When you use or share this project, **please**
 **be responsible to the ecosystem** ;)

**IEKO Team**

--------
© Copyright 2021 IEKO - All rights reserved. | https://ieko.io/
