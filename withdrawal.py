import requests
from algorand_sdk import AlgorandSDK
from utilities import exec_and_catch, check_needed_data
from settings import (
    PUBLIC_API_KEY, PRIVATE_API_KEY, BASE_URL,
    BC_INTEGRATION_ID, WITHDRAWAL_AMOUNT, WITHDRAWAL_CURRENCY, WALLET_RECEIVER
)

needed_data = [PUBLIC_API_KEY, PRIVATE_API_KEY, BASE_URL]
# Check if all required data is filled
check_needed_data(needed_data)

# Authentication data for SixClovers
authentication = (PUBLIC_API_KEY, PRIVATE_API_KEY)
withdrawal_endpoint = "transactions/withdraw"
withdrawal_payload = {
    "amount": WITHDRAWAL_AMOUNT,
    "currency": WITHDRAWAL_CURRENCY,
    "beneficiary": {
        "wallet": {
            "address": WALLET_RECEIVER # wallet that receives the funds
        }
    },
    "originator": {
        "organization": {
            # integration id of the multisig SixClovers wallet
            "blockchainIntegrationId": BC_INTEGRATION_ID
        }
    }
}

# Post withdrawal to SixClovers
withdrawal_response = requests.post(
    BASE_URL + withdrawal_endpoint,
    auth=authentication,
    json=withdrawal_payload
)
if withdrawal_response.status_code != 200:
    raise Exception("Failed sending withdrawal to SixClovers")

tx = withdrawal_response.json()
# Get the transaction ID
tx_id = tx["transactionId"]
# Get the reference token & signature payloads to commit the transaction
reference_token = tx["referenceToken"]
signature_payload = tx["signaturePayloads"]

# Decoding, signing and encoding payloads with Algorand SDK
decoded_transactions = exec_and_catch(signature_payload, AlgorandSDK.decodeSignaturePayloads)
signed_transactions = exec_and_catch(decoded_transactions, AlgorandSDK.signAndEncodeTransactions)

# Commit transaction to SixClovers
commit_endpoint = f"transactions/{tx_id}/commit"
commit_payload = {
    "signaturePayloads": signed_transactions,
    "referenceToken": reference_token
}
commit_response = requests.post(
    BASE_URL + commit_endpoint,
    auth=authentication,
    json=commit_payload
)
if commit_response.status_code not in [200, 202]:
    raise Exception(f"Error commiting {commit_response.status_code}")

print(f"Commited successfully - status {commit_response.status_code}")
