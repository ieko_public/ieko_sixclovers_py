def check_needed_data(needed_data):
    # Check if all required data is filled
    if None in needed_data or "" in needed_data:
        raise Exception("ERROR - Environment file is not complete, script will not run.")

def exec_and_catch(payload, function):
    # abstract the function to be called and catch possible errors
    response = function(payload)
    if response["error"]:
        raise Exception(response["data"])
    return response["data"]