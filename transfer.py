from tabnanny import check
import requests
from algorand_sdk import AlgorandSDK
from utilities import exec_and_catch, check_needed_data
from settings import (
    TRANSFER_AMOUNT, TRANSFER_CURRENCY, EMAIL_IDENTITY_RECEIVER, 
    ORGANIZATION_ID_RECEIVER, ORIGINATOR_ACCOUNT_ID, PRIVATE_API_KEY,
    PUBLIC_API_KEY, PUBLIC_API_KEY_RECEIVER, PRIVATE_API_KEY_RECEIVER,
    BASE_URL
)

needed_data = [
    TRANSFER_AMOUNT, TRANSFER_CURRENCY, EMAIL_IDENTITY_RECEIVER,
    ORGANIZATION_ID_RECEIVER, ORIGINATOR_ACCOUNT_ID, PRIVATE_API_KEY,
    PUBLIC_API_KEY, PUBLIC_API_KEY_RECEIVER, PRIVATE_API_KEY_RECEIVER,
    BASE_URL
]
# Check if all required data is filled
check_needed_data(needed_data)

# Authentication data for SixClovers
authentication = (PUBLIC_API_KEY, PRIVATE_API_KEY)
transfer_endpoint = f"transactions/transfer"
transfer_payload = {
    "amount": TRANSFER_AMOUNT,
    "currency": TRANSFER_CURRENCY,
    "beneficiary": {
        "amount": TRANSFER_AMOUNT,
        "currency": TRANSFER_CURRENCY,
        "identity": {
            # identity of the receiver, there are many options, choose your own
            "type": "EMAIL_ADDRESS",
            "value": EMAIL_IDENTITY_RECEIVER
        },
        "organization": {
            "organizationId": ORGANIZATION_ID_RECEIVER
        }
    },
    "originator": {
        "account": {
            "accountId": ORIGINATOR_ACCOUNT_ID
        },
        "amount": TRANSFER_AMOUNT,
        "currency": TRANSFER_CURRENCY
    },
    "ipAddress": "127.0.0.1",
    "platform": "BROWSER_DESKTOP"
}

# Post transfer to SixClovers
transfer_response = requests.post(
    BASE_URL + transfer_endpoint,
    auth=authentication,
    json=transfer_payload
)
if transfer_response.status_code not in [200, 202]:
    raise Exception(f"Error {transfer_response.status_code} - {transfer_response.json()}")

transfer_response = transfer_response.json()
# Get the transaction ID
tx_id = transfer_response["transactionId"]

# Approve transfer (with beneficiary organization)
approve_endpoint = f"transactions/{tx_id}/approve"
authentication_approve = (PUBLIC_API_KEY_RECEIVER, PRIVATE_API_KEY_RECEIVER)
approve_response = requests.post(
    BASE_URL + approve_endpoint,
    auth=authentication_approve
)
if approve_response.status_code not in [200, 202]:
    raise Exception(f"Error {approve_response.status_code} - {approve_response.json()}")

# Complete transfer (switch to originator organization)
complete_endpoint = f"transactions/{tx_id}/complete"
complete_response = requests.post(
    BASE_URL + complete_endpoint,
    auth=authentication,
    json={}
)
if complete_response.status_code not in [200, 202]:
    raise Exception(f"Error {complete_response.status_code} - {complete_response.json()}")

complete_response = complete_response.json()
# Get the reference token & signature payloads to commit transaction
reference_token = complete_response["referenceToken"]
signature_payloads = complete_response["signaturePayloads"]

# Decoding, signing and encoding payloads with Algorand SDK
decoded_transactions = exec_and_catch(signature_payloads, AlgorandSDK.decodeSignaturePayloads)
signed_transactions = exec_and_catch(decoded_transactions, AlgorandSDK.signAndEncodeTransactions)

# Commit transaction to SixClovers
commit_endpoint = f"transactions/{tx_id}/commit"
commit_payload = {
    "signaturePayloads": signed_transactions,
    "referenceToken": reference_token
}

commit_response = requests.post(
    BASE_URL + commit_endpoint,
    auth=authentication,
    json=commit_payload
)
if commit_response.status_code not in [200, 202]:
    raise Exception(f"Error {commit_response.status_code}")

print(f"Commited successfully - status {commit_response.status_code}")
